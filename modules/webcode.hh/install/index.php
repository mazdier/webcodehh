<?php
defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();

class webcode_hh extends CModule
{
	
	public $MODULE_ID;
	public $MODULE_VERSION;
	public $MODULE_VERSION_DATE;
	public $MODULE_NAME;
	public $MODULE_DESCRIPTION;
	public $MODULE_GROUP_RIGHTS;
	public $PARTNER_NAME;
	public $PARTNER_URI;
	public $SHOW_SUPER_ADMIN_GROUP_RIGHTS;
	
	public function __construct(){
		$arModuleVersion=array();
		include(__DIR__."/version.php");
		$this->MODULE_ID = 'webcode.hh';
		$this->MODULE_VERSION =$arModuleVersion['VERSION'] ;
		$this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
		$this->MODULE_NAME = "webcode.hh";
		$this->MODULE_DESCRIPTION = 'Модуль интеграции c headhunter';
		$this->MODULE_GROUP_RIGHTS = 'Y';
		$this->SHOW_SUPER_ADMIN_GROUP_RIGHTS = 'Y';
		$this->PARTNER_NAME = 'SERVIT';
		$this->PARTNER_URI = "http://webcode.website";
	}
	
	function isVersionD7(){
		return CheckVersion( \Bitrix\Main\ModuleManager::getVersion('main'),'14.00.00');

	}
	
	function InstallEvents(){
		RegisterModule($this->MODULE_ID);
		RegisterModuleDependences("main", "OnBeforeProlog", $this->MODULE_ID, "\\Webcode\\HH\\Loader", "IncludeFile");
		return true;
	}
	
	function UnInstallEvents(){
		
		UnRegisterModuleDependences("main", "OnBeforeProlog", $this->MODULE_ID, "\\Webcode\\HH\\Loader", "IncludeFile");
		UnRegisterModule($this->MODULE_ID);
		return true;
	}
	
	function InstallFiles(){
		if ($this->isVersionD7()) {
			CopyDirFiles(__DIR__ . "/files",
				$_SERVER["DOCUMENT_ROOT"]."/local", true, true);
			return true;
		}
	}
	
	function UnInstallFiles(){
		if ($this->isVersionD7()) {
			DeleteDirFiles(__DIR__ . "/files",
				$_SERVER["DOCUMENT_ROOT"]."/local", true, true);
			return true;
		}
	}
	
	function AddUrl(){
		 include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');
		 $UrlObj = new \Bitrix\Main\UrlRewriter;
		 if(count($UrlObj->getList('s1',['ID'=>'local:webcode.hh.view']))==0) {
               $UrlObj->add(SITE_ID, [
                   'CONDITION' => '#^/pub/HHApi/([0-9a-zA-Z/_]+)#',
                   'RULE' => '',
                   'ID' => 'local:webcode.hh.view',
                   'PATH' => '/pub/apihh.php',
                   'SORT' => 1,
               ]);
		 }
	}
	
	function DeleteUrl(){
		include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');
		$UrlObj = new \Bitrix\Main\UrlRewriter;
		$UrlObj->delete(SITE_ID, [
			   'CONDITION' => '#^/pub/HHApi/([0-9a-zA-Z/_]+)#',
			   'RULE' => '',
			   'ID' => 'local:webcode.hh.view',
			   'PATH' => '/pub/apihh.php',
			   'SORT' => 1,
		]);
	}
	
	function AddTemplateUrl(){
		
	}
	
	public function doInstall(){
		$this->InstallFiles();
		$this->InstallEvents();
	}

	public function doUninstall(){
		$this->UnInstallFiles();
		$this->UnInstallEvents();
	}


}
?>