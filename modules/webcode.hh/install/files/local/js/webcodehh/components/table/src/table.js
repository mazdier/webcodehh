import 'main.polyfill.intersectionobserver';
import {Vue} from 'ui.vue';
import  {usertypes} from 'webcodehh.components.usertypes';

Vue.directive('scroll', {
	inserted: function (el, binding) {
		let f = function (evt) {
			if (binding.value(evt, el)) {
				window.removeEventListener('scroll', f)
			}
		}
		window.addEventListener('scroll', f)
	}
})

Vue.component('tablehh',
{
	props: ['urlApi'],
	components:{
		usertypes
	},
	data:function (){
		return {
			sortIndex:['id','salary','contenthh','employer','url'],
			filters:{},
			TableData:{},
			loaded:false,
			loaderShow:false,
			currency:{},
			currencyDefault:{id: "RUR", name: "Рубли", rate: 1},
			sortParam:'price',
		}
	},
	methods:{
		GetData(filters, loader=false){
			if(typeof(filters.currency) !== 'undefined')
				if(typeof(filters.currency.data) !== 'undefined')
					this.currency = filters.currency;
			let _self=this;
			let esc = encodeURIComponent;
			if(loader){
				if(Object.keys(this.TableData).length !==0 ){
					this.filters.page += 1;
				}
				else {
					this.PrepareFilters(filters);
				}
			}
			else this.PrepareFilters(filters);
			
			
			let query = Object.keys(this.filters)
				.map(k => esc(k) + '=' + esc(this.filters[k]))
				.join('&');
				
			   BX.ajax({
				   url: urlApi+'GetVacancies?'+query,
				   data: {
				   },
				   method: 'GET',
				   dataType: 'json',
				   timeout: 30,
				   async: true,
				   onsuccess: function(TableData){
					   if(!loader) _self.TableData={};
					   if(Object.keys(_self.TableData).length !==0 ){
						  Array.prototype.unshift.apply(TableData.items,_self.TableData.items)
					   }
					  _self.SortData(TableData);
					  _self.loaderShow=false;
					   _self.$root.$emit('loaded',_self.loaded);
				   },
				   onfailure: function(){
					   _self.filters={"error":true};
					   _self.errorTxt="Ошибка соединения";
				   }
			   });
		},
		PrepareFilters(filters){
			let NewFilters={};
			for (var key in filters) {
				if(typeof(filters[key].selected)!='undefined')
					NewFilters[key]=filters[key].selected;
			};
			NewFilters.page=1;
				this.filters = NewFilters;
		},
		SortData(TableData){
			if(typeof(this.filters.currency) !==  'undefined'){
					this.currencyDefault = this.currency.data[this.currency.data.findIndex(elem => elem.id ===  this.currency.selected)];
			}
			for (var index in TableData.items) {
				var	TableD = {};
				this.sortIndex.forEach(sortVal => {
					if(sortVal == 'salary')
						if(typeof(this.filters.currency) !==  'undefined')
							if(typeof(TableData.items[index][sortVal]) !==  'undefined')
								if(typeof(TableData.items[index][sortVal].data) !== 'undefined')
									if(TableData.items[index][sortVal].data.amount){
										var AmountRUR = TableData.items[index][sortVal].data.amount/(this.currency.data[this.currency.data.findIndex(elem => elem.id ===  TableData.items[index][sortVal].data.id)])['rate'];
										TableData.items[index][sortVal].data.id = this.currencyDefault.id;
										TableData.items[index][sortVal].data.amount = this.currencyDefault.rate*AmountRUR;
									}
							
					TableD[sortVal] = TableData.items[index][sortVal]
				});
				TableData.items[index] = TableD;
			}
			var	TableDHeaders = {};
			this.sortIndex.forEach(sortVal => {
				TableDHeaders[sortVal] = TableData.headers[sortVal];
			});
			TableData.headers = TableDHeaders;
			this.TableData = TableData;
		},
		handleScroll: function (evt, el) {
			let Ycord = this.$refs.loader.getBoundingClientRect().y;
			if (Ycord >= 200 && Ycord <= 682&&this.loaderShow === false){
				 if(this.TableData.pages >= (this.TableData.page +1) ){
					this.GetData(this.filters,true);
					this.loaderShow=true;
				}
				
			}
		},
		sortByPrice:function(d1, d2){
			if((typeof(d1.salary.data) !== 'undefined') && (typeof(d2.salary.data) !== 'undefined' )){
				return (d1.salary.data.amount > d2.salary.data.amount) ? -1 : 1; 
			}
			else if((typeof(d1.salary.data) !== 'undefined') && (typeof(d2.salary.data) === 'undefined' )) return -1;
			else if((typeof(d1.salary.data) === 'undefined') && (typeof(d2.salary.data) !== 'undefined' )) return 1;
			else {
				return 0 ;
			}
		}
	},
	computed:{
		sortedList () {
			if(typeof(this.TableData.items) !== 'undefined')
				switch(this.sortParam){
					case 'price': 
						return this.TableData.items.sort(this.sortByPrice);
				}
			else return this.TableData.items;
		 }
    },
	created() {
		this.$root.$on('setfilters', this.GetData);
    },
	template:`
		<table class="table table-hover " style="marginTop:20px">
			<thead>
				<tr>
					<th scope="col" v-for="(label, name_key) in TableData.headers" >
						<div :id="name_key" >
							{{label}}
						</div>
					</th>
				</tr>
			</thead>
			<tbody>
				<tr v-for="item in sortedList">
					<td v-for="(val, key) in item">
						<div>
							<usertypes :type="val.type" :data="val.data">
							</usertypes> 
						</div>
					</td>
				</tr>
				<tr align="center">
					<td colspan="6">
						<div ref="loader" v-scroll="handleScroll" >
							<div v-if="loaderShow" class="align:center">
								...LOADIG...
							</div>
						</div>
					</td>
				</tr>
			  </tbody>
		</table>
	`
});