<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
{
	die();
}

return [
	'css' => 'dist/filtershh.bundle.css',
	'js' => 'dist/filtershh.bundle.js',
	'rel' => [
		'main.polyfill.core',
		'main.polyfill.intersectionobserver',
		'ui.vue',
		'ui.forms',
		'ui.vue.components.datepick',
		'webcodehh.components.modal',
	],
	'skip_core' => true,
];