<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
{
	die();
}

return [
	'css' => 'dist/table.bundle.css',
	'js' => 'dist/table.bundle.js',
	'rel' => [
		'main.polyfill.core',
		'main.polyfill.intersectionobserver',
		'ui.vue',
		'webcodehh.components.usertypes',
	],
	'skip_core' => true,
];