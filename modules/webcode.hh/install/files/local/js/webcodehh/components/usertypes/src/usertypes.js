import 'main.polyfill.intersectionobserver';
import {Vue} from 'ui.vue';
import  './style.css';

var templatestr=Vue.component('review',{
	props:['name'],
	data: function() {
		return {
			NameEmp:this.name,
			IdReview:'',
		}
	},
	methods:{
		GetReview:function(){
			   BX.SidePanel.Instance.open("IsReview?name="+this.NameEmp);
		},
	},
	template:`
		<div  @click="GetReview()">
			<font color="#196eb2">{{NameEmp}}</font>
		</div>
	`
});

var templatestr=Vue.component('templatestr',{
		props:['templatestring','data'],
	data: function() {
		return {
			template: null
		}
	},
	render: function(createElement) {
		if (!this.template) 
			return createElement('div');
		else return this.template();
	},
	mounted() {
		if(typeof(this.templatestring) !== 'undefined'){
			if(this.templatestring)
				this.template = Vue.compile(this.templatestring).render;
		}
		else this.template = Vue.compile('<div>{{data}}</div>').render;
	}
});



let usertypes=Vue.component('usertypes',
{
	props: ['type','data'],
	components:{
		templatestr,
		data:''
	},
	data:function (){
		return {
			content:'',
			h:'<div>hello</div>',
			newdata:this.data,
			ArrTemplate: {
				employer:`<div ><img v-if="data.logo" :src="data.logo" ><review :name="data.name"></review></div>`,//
				urlvac: `<div @click="$root.$emit('GetJob',data.id)"><font color="#196eb2">{{data.name}}</font></div>`,
				currency:`<div>{{data?(data.amount?data.amount+' '+data.id:''):''}}</div>`,
				contenthh:`<div v-html="data.reduce(function(allcontent, current) {
																	return allcontent + current;
												}, ' ')"></div>
				`,
				
			}
		}
	},
	methods:{
		PrepareContent:function(){
			if (typeof(this.ArrTemplate[this.type]) !== "undefined") {
				if (typeof(this.data) !== "undefined") {
					return this.ArrTemplate[this.type];
				}
				else return '';
			}
			else return this.data;
		},
		GetReview:function(name){
			console.log(name);
			return name;
		},
		
	},
	created:function(){
		this.content = this.PrepareContent();
	},
	template:`
		<div class="usertypes">
				<templatestr :templatestring="ArrTemplate[type]" :data = "newdata"></templatestr>
		</div>
	`
});

export{
	usertypes
}