this.BX = this.BX || {};
this.BX.Webcodehh = this.BX.Webcodehh || {};
(function (exports,main_polyfill_intersectionobserver,ui_vue) {
  'use strict';

  ui_vue.Vue.directive('click-outside', {
    bind: function bind(el, binding, vnode) {
      el.clickOutsideEvent = function (event) {
        if (!(el == event.target || el.contains(event.target))) {
          vnode.context[binding.expression](event);
        }
      };

      document.body.addEventListener('click', el.clickOutsideEvent);
    },
    unbind: function unbind(el) {
      document.body.removeEventListener('click', el.clickOutsideEvent);
    }
  });
  var modal = ui_vue.Vue.component('modal', {
    props: ['el', 'show'],
    data: function data() {
      return {
        coords: {},
        style: ''
      };
    },
    methods: {
      closeEvent: function closeEvent(event) {
        var DatePicker = document.getElementsByClassName('vdpComponent');
        var flag = false;

        if (DatePicker.length != 0) {
          if (DatePicker[0].contains(event.target)) flag = true;
        }

        if (event.target == this.el || flag) ; else {
          this.$emit('close');
          this.el.focus();
        }
      }
    },
    template: "\n          <div  class=\"modal-wrapper\" >\n            <div ref=\"modal\" class=\"modal-container\" :style=\"\" v-click-outside=\"closeEvent\">\n\n              <div class=\"modal-header\">\n                <slot name=\"header\">\n                </slot>\n              </div>\n\n              <div class=\"modal-body\">\n                <slot name=\"body\">\n                </slot>\n              </div>\n\n              <div class=\"modal-footer\">\n                <slot name=\"footer\">\n                  <button class=\"ui-btn ui-btn-xs\" @click=\"$emit('close');el.focus()\">\n                    OK\n                  </button>\n                </slot>\n              </div>\n            </div>\n          </div>\n\t"
  });

  exports.modal = modal;

}((this.BX.Webcodehh.Components = this.BX.Webcodehh.Components || {}),BX,BX));
//# sourceMappingURL=modal.bundle.js.map
