this.BX = this.BX || {};
this.BX.Webcodehh = this.BX.Webcodehh || {};
(function (exports,main_polyfill_intersectionobserver,ui_vue,ui_forms,ui_vue_components_datepick,webcodehh_components_modal) {
  'use strict';

  ui_vue.Vue.component('filtershh', {
    props: ['urlApi'],
    data: function data() {
      return {
        filters: {
          error: false,
          text: {
            selected: 'bitrix'
          }
        },
        el: '',
        showModal: false,
        errorTxt: '',
        currency: {},
        datePicker: false,
        date: '',
        loader: false
      };
    },
    components: {
      modal: webcodehh_components_modal.modal,
      DatePick: ui_vue_components_datepick.DatePick
    },
    methods: {
      ModalWindow: function ModalWindow() {
        this.el = this.$refs.filter;
        this.showModal = !this.showModal;
        this.el.focus();
      },
      EmitData: function EmitData() {
        if (this.filters.text.selected.length == 0) {
          this.filters.error = true;
          this.errorTxt = "Не введен текс для поиска";
        } else {
          this.loader = true;
          this.$root.$emit('setfilters', this.filters);
          this.filters.error = false;
        }
      },
      ShowDatePicker: function ShowDatePicker(key) {
        var _self = this;

        var datenow = new Date();
        if (!this.datePicker) this.datePicker = new BX.UI.Vue.Components.DatePick({
          node: _self.$refs.dateP,
          hasTime: false,
          events: {
            change: function (value) {
              var arDate = value.split('.');
              var dateselect = new Date(arDate[1] + '/' + arDate[0] + '/' + arDate[2]);
              var daysLag = Math.ceil(Math.abs(dateselect.getTime() - datenow.getTime()) / (1000 * 3600 * 24));
              _self.filters[key].selected = daysLag;
              _self.date = daysLag;
            }.bind(this)
          }
        });
        this.datePicker.value = _self.$refs.dateP.value;
        this.datePicker.toggle();
      },
      Loaded: function Loaded(state) {
        this.loader = state;
      }
    },
    mounted: function mounted() {
      this.$root.$on('loaded', this.Loaded);
    },
    beforeMount: function beforeMount() {
      var _self = this;

      BX.ajax({
        url: urlApi + 'GetFilters',
        data: {},
        method: 'GET',
        dataType: 'json',
        timeout: 30,
        onsuccess: function onsuccess(filters) {
          filters.text = {
            selected: _self.filters.text.selected
          };
          _self.filters = filters;
          _self.filters.order_by = filters.vacancy_search_order;
          delete _self.filters.vacancy_search_order;
          _self.currency = _self.filters.currency;
        },
        onfailure: function onfailure() {
          _self.filters = {
            "error": true
          };
          _self.errorTxt = "Ошибка соединения";
        }
      });
    },
    template: "\n\t\t <div class=\"ui-ctl ui-ctl-textbox ui-ctl-before-icon ui-ctl-after-icon\">\n            <div class=\"ui-ctl-tag ui-ctl-tag-success\" v-if=\"!filters.error\">\u041F\u043E\u0438\u0441\u043A \u0438 \u0444\u0438\u043B\u044C\u0442\u0440\u0430\u0446\u0438\u044F</div>\n            <div class=\"ui-ctl-tag ui-ctl-tag-danger\"  v-if=\"filters.error\">{{errorTxt}}</div>\n                <div class=\"ui-ctl-before ui-ctl-icon-search\" v-if=\"!loader\" ></div>\n\t\t\t\t<div class=\"ui-ctl-before ui-ctl-icon-loader\" v-if=\"loader\"></div>\n                <button class=\"ui-ctl-after ui-ctl-icon-clear\" ></button>\n                <input ref=\"filter\"  type=\"text\" class=\"ui-ctl-element\" v-model=\"filters.text.selected\" @click=\"ModalWindow\">\n\t\t\t\t\t<modal :el=\"el\" v-if=\"showModal\" @close=\"showModal = false;EmitData();\" >\n\t\t\t\t\t\t<h3 slot=\"header\">{{filters.text.selected}}</h3>\n\t\t\t\t\t\t<div slot=\"body\">\n\t\t\t\t\t\t\t<table>\n\t\t\t\t\t\t\t\t<tr v-for=\"(filter,key) in filters\">\n\t\t\t\t\t\t\t\t\t<td>{{filter.name}}</td>\n\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t<div class=\"ui-ctl ui-ctl-after-icon ui-ctl-dropdown ui-ctl-xs\" v-if=\"filter.type=='dictionaries' || filter.type=='currency'\">\n\t\t\t\t\t\t\t\t\t\t\t<div class=\"ui-ctl-after ui-ctl-icon-angle\"></div>\n\t\t\t\t\t\t\t\t\t\t\t<select class=\"ui-ctl-element\" v-model=\"filter.selected\">\n\t\t\t\t\t\t\t\t\t\t\t\t  <option v-for=\"option in filter.data\" v-bind:value=\"option.id\">\n\t\t\t\t\t\t\t\t\t\t\t\t\t{{ option.name }}\n\t\t\t\t\t\t\t\t\t\t\t\t  </option>\n\t\t\t\t\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t\t\t\t</div\">\n\t\t\t\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t\t\t\t<div v-if=\"filter.type==='boolean'\">\n\t\t\t\t\t\t\t\t\t\t\t<label class=\"ui-ctl ui-ctl-checkbox\" :for=\"key\">\n\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"checkbox\" :id=\"key\" class=\"ui-ctl-element\" v-model=\"filter.selected\">\n\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"ui-ctl-label-text\">{{filter.name}}</div>\n\t\t\t\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t\t<div  class=\"ui-ctl ui-ctl-textbox ui-ctl-xs\" v-if=\"filter.type==='daypicker'\" @click=\"ShowDatePicker(key)\">\n\t\t\t\t\t\t\t\t\t\t\t<input class=\"ui-ctl-element\" type=\"text\" ref=\"dateP\" v-model=\"date\">\n\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t\t<div  class=\"ui-ctl ui-ctl-textbox ui-ctl-xs\" v-if=\"filter.type==='int'\" >\n\t\t\t\t\t\t\t\t\t\t\t<input class=\"ui-ctl-element\" type=\"text\"  v-model=\"filter.selected\">\n\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t</table>\n\t\t\t\t\t\t</div>\t\n\t\t\t\t\t</modal>\n\t\t\t\t\t\n            </div>\n\t"
  });

}((this.BX.Webcodehh.Components = this.BX.Webcodehh.Components || {}),BX,BX,BX,BX,BX.Webcodehh.Components));
//# sourceMappingURL=filtershh.bundle.js.map
