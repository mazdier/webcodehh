<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
{
	die();
}

return [
	'css' => 'dist/modal.bundle.css',
	'js' => 'dist/modal.bundle.js',
	'rel' => [
		'main.polyfill.core',
		'main.polyfill.intersectionobserver',
		'ui.vue',
		'ui.buttons',
	],
	'skip_core' => true,
];