this.BX = this.BX || {};
this.BX.Webcodehh = this.BX.Webcodehh || {};
(function (exports,main_polyfill_intersectionobserver,ui_vue,webcodehh_components_usertypes) {
	'use strict';

	ui_vue.Vue.directive('scroll', {
	  inserted: function inserted(el, binding) {
	    var f = function f(evt) {
	      if (binding.value(evt, el)) {
	        window.removeEventListener('scroll', f);
	      }
	    };

	    window.addEventListener('scroll', f);
	  }
	});
	ui_vue.Vue.component('tablehh', {
	  props: ['urlApi'],
	  components: {
	    usertypes: webcodehh_components_usertypes.usertypes
	  },
	  data: function data() {
	    return {
	      sortIndex: ['id', 'salary', 'contenthh', 'employer', 'url'],
	      filters: {},
	      TableData: {},
	      loaded: false,
	      loaderShow: false,
	      currency: {},
	      currencyDefault: {
	        id: "RUR",
	        name: "Рубли",
	        rate: 1
	      },
	      sortParam: 'price'
	    };
	  },
	  methods: {
	    GetData: function GetData(filters) {
	      var _this = this;

	      var loader = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
	      if (typeof filters.currency !== 'undefined') if (typeof filters.currency.data !== 'undefined') this.currency = filters.currency;

	      var _self = this;

	      var esc = encodeURIComponent;

	      if (loader) {
	        if (Object.keys(this.TableData).length !== 0) {
	          this.filters.page += 1;
	        } else {
	          this.PrepareFilters(filters);
	        }
	      } else this.PrepareFilters(filters);

	      var query = Object.keys(this.filters).map(function (k) {
	        return esc(k) + '=' + esc(_this.filters[k]);
	      }).join('&');
	      BX.ajax({
	        url: urlApi + 'GetVacancies?' + query,
	        data: {},
	        method: 'GET',
	        dataType: 'json',
	        timeout: 30,
	        async: true,
	        onsuccess: function onsuccess(TableData) {
	          if (!loader) _self.TableData = {};

	          if (Object.keys(_self.TableData).length !== 0) {
	            Array.prototype.unshift.apply(TableData.items, _self.TableData.items);
	          }

	          _self.SortData(TableData);

	          _self.loaderShow = false;

	          _self.$root.$emit('loaded', _self.loaded);
	        },
	        onfailure: function onfailure() {
	          _self.filters = {
	            "error": true
	          };
	          _self.errorTxt = "Ошибка соединения";
	        }
	      });
	    },
	    PrepareFilters: function PrepareFilters(filters) {
	      var NewFilters = {};

	      for (var key in filters) {
	        if (typeof filters[key].selected != 'undefined') NewFilters[key] = filters[key].selected;
	      }
	      NewFilters.page = 1;
	      this.filters = NewFilters;
	    },
	    SortData: function SortData(TableData) {
	      var _this2 = this;

	      if (typeof this.filters.currency !== 'undefined') {
	        this.currencyDefault = this.currency.data[this.currency.data.findIndex(function (elem) {
	          return elem.id === _this2.currency.selected;
	        })];
	      }

	      for (var index in TableData.items) {
	        var TableD = {};
	        this.sortIndex.forEach(function (sortVal) {
	          if (sortVal == 'salary') if (typeof _this2.filters.currency !== 'undefined') if (typeof TableData.items[index][sortVal] !== 'undefined') if (typeof TableData.items[index][sortVal].data !== 'undefined') if (TableData.items[index][sortVal].data.amount) {
	            var AmountRUR = TableData.items[index][sortVal].data.amount / _this2.currency.data[_this2.currency.data.findIndex(function (elem) {
	              return elem.id === TableData.items[index][sortVal].data.id;
	            })]['rate'];

	            TableData.items[index][sortVal].data.id = _this2.currencyDefault.id;
	            TableData.items[index][sortVal].data.amount = _this2.currencyDefault.rate * AmountRUR;
	          }
	          TableD[sortVal] = TableData.items[index][sortVal];
	        });
	        TableData.items[index] = TableD;
	      }

	      var TableDHeaders = {};
	      this.sortIndex.forEach(function (sortVal) {
	        TableDHeaders[sortVal] = TableData.headers[sortVal];
	      });
	      TableData.headers = TableDHeaders;
	      this.TableData = TableData;
	    },
	    handleScroll: function handleScroll(evt, el) {
	      var Ycord = this.$refs.loader.getBoundingClientRect().y;

	      if (Ycord >= 200 && Ycord <= 682 && this.loaderShow === false) {
	        if (this.TableData.pages >= this.TableData.page + 1) {
	          this.GetData(this.filters, true);
	          this.loaderShow = true;
	        }
	      }
	    },
	    sortByPrice: function sortByPrice(d1, d2) {
	      if (typeof d1.salary.data !== 'undefined' && typeof d2.salary.data !== 'undefined') {
	        return d1.salary.data.amount > d2.salary.data.amount ? -1 : 1;
	      } else if (typeof d1.salary.data !== 'undefined' && typeof d2.salary.data === 'undefined') return -1;else if (typeof d1.salary.data === 'undefined' && typeof d2.salary.data !== 'undefined') return 1;else {
	        return 0;
	      }
	    }
	  },
	  computed: {
	    sortedList: function sortedList() {
	      if (typeof this.TableData.items !== 'undefined') switch (this.sortParam) {
	        case 'price':
	          return this.TableData.items.sort(this.sortByPrice);
	      } else return this.TableData.items;
	    }
	  },
	  created: function created() {
	    this.$root.$on('setfilters', this.GetData);
	  },
	  template: "\n\t\t<table class=\"table table-hover \" style=\"marginTop:20px\">\n\t\t\t<thead>\n\t\t\t\t<tr>\n\t\t\t\t\t<th scope=\"col\" v-for=\"(label, name_key) in TableData.headers\" >\n\t\t\t\t\t\t<div :id=\"name_key\" >\n\t\t\t\t\t\t\t{{label}}\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</th>\n\t\t\t\t</tr>\n\t\t\t</thead>\n\t\t\t<tbody>\n\t\t\t\t<tr v-for=\"item in sortedList\">\n\t\t\t\t\t<td v-for=\"(val, key) in item\">\n\t\t\t\t\t\t<div>\n\t\t\t\t\t\t\t<usertypes :type=\"val.type\" :data=\"val.data\">\n\t\t\t\t\t\t\t</usertypes> \n\t\t\t\t\t\t</div>\n\t\t\t\t\t</td>\n\t\t\t\t</tr>\n\t\t\t\t<tr align=\"center\">\n\t\t\t\t\t<td colspan=\"6\">\n\t\t\t\t\t\t<div ref=\"loader\" v-scroll=\"handleScroll\" >\n\t\t\t\t\t\t\t<div v-if=\"loaderShow\" class=\"align:center\">\n\t\t\t\t\t\t\t\t...LOADIG...\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</td>\n\t\t\t\t</tr>\n\t\t\t  </tbody>\n\t\t</table>\n\t"
	});

}((this.BX.Webcodehh.Components = this.BX.Webcodehh.Components || {}),BX,BX,BX.Webcodehh.Components));
//# sourceMappingURL=table.bundle.js.map
