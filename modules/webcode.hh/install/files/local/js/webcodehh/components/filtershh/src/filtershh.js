import 'main.polyfill.intersectionobserver';
import {Vue} from 'ui.vue';
import  'ui.forms';
import  {DatePick} from 'ui.vue.components.datepick';
import  './style.css';
import  {modal} from 'webcodehh.components.modal';



Vue.component('filtershh',
{
	props: ['urlApi'],
	data:function (){
		return {
			filters:{error: false,text:{selected:'bitrix'}},
			el:'',
			showModal:false,
			errorTxt:'',
			currency:{},
			datePicker:false,
			date:'',
			loader:false,
		}
	},
	components:{
		modal,
		DatePick
	},
	methods:{
		ModalWindow(){
			this.el = this.$refs.filter
			this.showModal = !this.showModal
			this.el.focus()
		},
		EmitData(){
			if(this.filters.text.selected.length==0){
				this.filters.error = true;
				this.errorTxt = "Не введен текс для поиска"
			}
			else{
				this.loader = true;
				this.$root.$emit('setfilters', this.filters);
				this.filters.error = false;
			}
		},
		ShowDatePicker(key){
			
			let _self = this
			let datenow = new Date();
			if(!this.datePicker)
				this.datePicker = new BX.UI.Vue.Components.DatePick(
					{
						node: _self.$refs.dateP,
						hasTime: false,
						events: {
							change: function(value){
								var arDate = value.split('.');
								var dateselect = new Date(arDate[1]+'/'+arDate[0]+'/'+arDate[2]);
								var daysLag = Math.ceil(Math.abs(dateselect.getTime() - datenow.getTime()) / (1000 * 3600 * 24));
								_self.filters[key].selected = daysLag;
								_self.date = daysLag;
							}.bind(this)
						}
					}
				);
			this.datePicker.value = _self.$refs.dateP.value;
			this.datePicker.toggle();
		},
		Loaded(state){
			this.loader = state;
		}
	},
	mounted() {
		this.$root.$on('loaded', this.Loaded);
    },
	beforeMount:function (){
           let _self=this;
           BX.ajax({
               url: urlApi+'GetFilters',
               data: {
               },
               method: 'GET',
               dataType: 'json',
               timeout: 30,
               onsuccess: function(filters){
				   filters.text = {selected:_self.filters.text.selected}
                   _self.filters=filters;
				   _self.filters.order_by = filters.vacancy_search_order;
				   delete _self.filters.vacancy_search_order;
				   _self.currency=_self.filters.currency;
               },
               onfailure: function(){
                   _self.filters={"error":true};
				   _self.errorTxt="Ошибка соединения";
               }
           });
       },
	template:`
		 <div class="ui-ctl ui-ctl-textbox ui-ctl-before-icon ui-ctl-after-icon">
            <div class="ui-ctl-tag ui-ctl-tag-success" v-if="!filters.error">Поиск и фильтрация</div>
            <div class="ui-ctl-tag ui-ctl-tag-danger"  v-if="filters.error">{{errorTxt}}</div>
                <div class="ui-ctl-before ui-ctl-icon-search" v-if="!loader" ></div>
				<div class="ui-ctl-before ui-ctl-icon-loader" v-if="loader"></div>
                <button class="ui-ctl-after ui-ctl-icon-clear" ></button>
                <input ref="filter"  type="text" class="ui-ctl-element" v-model="filters.text.selected" @click="ModalWindow">
					<modal :el="el" v-if="showModal" @close="showModal = false;EmitData();" >
						<h3 slot="header">{{filters.text.selected}}</h3>
						<div slot="body">
							<table>
								<tr v-for="(filter,key) in filters">
									<td>{{filter.name}}</td>
									<td>
										<div class="ui-ctl ui-ctl-after-icon ui-ctl-dropdown ui-ctl-xs" v-if="filter.type=='dictionaries' || filter.type=='currency'">
											<div class="ui-ctl-after ui-ctl-icon-angle"></div>
											<select class="ui-ctl-element" v-model="filter.selected">
												  <option v-for="option in filter.data" v-bind:value="option.id">
													{{ option.name }}
												  </option>
											</select>
										</div">
											
										<div v-if="filter.type==='boolean'">
											<label class="ui-ctl ui-ctl-checkbox" :for="key">
												<input type="checkbox" :id="key" class="ui-ctl-element" v-model="filter.selected">
												<div class="ui-ctl-label-text">{{filter.name}}</div>
											</label>
										</div>
										<div  class="ui-ctl ui-ctl-textbox ui-ctl-xs" v-if="filter.type==='daypicker'" @click="ShowDatePicker(key)">
											<input class="ui-ctl-element" type="text" ref="dateP" v-model="date">
										</div>
										<div  class="ui-ctl ui-ctl-textbox ui-ctl-xs" v-if="filter.type==='int'" >
											<input class="ui-ctl-element" type="text"  v-model="filter.selected">
										</div>
									</td>
								</tr>
							</table>
						</div>	
					</modal>
					
            </div>
	`
});
