this.BX = this.BX || {};
this.BX.Webcodehh = this.BX.Webcodehh || {};
(function (exports,main_polyfill_intersectionobserver,ui_vue) {
	'use strict';

	var templatestr = ui_vue.Vue.component('review', {
	  props: ['name'],
	  data: function data() {
	    return {
	      NameEmp: this.name,
	      IdReview: ''
	    };
	  },
	  methods: {
	    GetReview: function GetReview() {
	      BX.SidePanel.Instance.open("IsReview?name=" + this.NameEmp);
	    }
	  },
	  template: "\n\t\t<div  @click=\"GetReview()\">\n\t\t\t<font color=\"#196eb2\">{{NameEmp}}</font>\n\t\t</div>\n\t"
	});
	var templatestr = ui_vue.Vue.component('templatestr', {
	  props: ['templatestring', 'data'],
	  data: function data() {
	    return {
	      template: null
	    };
	  },
	  render: function render(createElement) {
	    if (!this.template) return createElement('div');else return this.template();
	  },
	  mounted: function mounted() {
	    if (typeof this.templatestring !== 'undefined') {
	      if (this.templatestring) this.template = ui_vue.Vue.compile(this.templatestring).render;
	    } else this.template = ui_vue.Vue.compile('<div>{{data}}</div>').render;
	  }
	});
	var usertypes = ui_vue.Vue.component('usertypes', {
	  props: ['type', 'data'],
	  components: {
	    templatestr: templatestr,
	    data: ''
	  },
	  data: function data() {
	    return {
	      content: '',
	      h: '<div>hello</div>',
	      newdata: this.data,
	      ArrTemplate: {
	        employer: "<div ><img v-if=\"data.logo\" :src=\"data.logo\" ><review :name=\"data.name\"></review></div>",
	        //
	        urlvac: "<div @click=\"$root.$emit('GetJob',data.id)\"><font color=\"#196eb2\">{{data.name}}</font></div>",
	        currency: "<div>{{data?(data.amount?data.amount+' '+data.id:''):''}}</div>",
	        contenthh: "<div v-html=\"data.reduce(function(allcontent, current) {\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\treturn allcontent + current;\n\t\t\t\t\t\t\t\t\t\t\t\t}, ' ')\"></div>\n\t\t\t\t"
	      }
	    };
	  },
	  methods: {
	    PrepareContent: function PrepareContent() {
	      if (typeof this.ArrTemplate[this.type] !== "undefined") {
	        if (typeof this.data !== "undefined") {
	          return this.ArrTemplate[this.type];
	        } else return '';
	      } else return this.data;
	    },
	    GetReview: function GetReview(name) {
	      console.log(name);
	      return name;
	    }
	  },
	  created: function created() {
	    this.content = this.PrepareContent();
	  },
	  template: "\n\t\t<div class=\"usertypes\">\n\t\t\t\t<templatestr :templatestring=\"ArrTemplate[type]\" :data = \"newdata\"></templatestr>\n\t\t</div>\n\t"
	});

	exports.usertypes = usertypes;

}((this.BX.Webcodehh.Components = this.BX.Webcodehh.Components || {}),BX,BX));
//# sourceMappingURL=usertypes.bundle.js.map
