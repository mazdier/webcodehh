import 'main.polyfill.intersectionobserver';
import {Vue} from 'ui.vue';
import  'ui.buttons';
import  './style.css';

 Vue.directive('click-outside', {
  bind: function (el, binding, vnode) {
    el.clickOutsideEvent = function (event) {
      if (!(el == event.target || el.contains(event.target))) {
        vnode.context[binding.expression](event);
      }
    };
    document.body.addEventListener('click', el.clickOutsideEvent)
  },
  unbind: function (el) {
    document.body.removeEventListener('click', el.clickOutsideEvent)
  },
});

let modal=Vue.component('modal',
{
	props: ['el','show'],
	data:function (){
		return {
			coords:{},
			style:''
		}
	},
	methods:{
		closeEvent:function (event) {
			let DatePicker = document.getElementsByClassName('vdpComponent');
			let flag=false;
			if(DatePicker.length!=0) {
				if( DatePicker[0].contains(event.target))
					flag = true;
			}
			if(event.target == this.el ||  flag ){
			}
			else {
				this.$emit('close');
				this.el.focus(); 
			}
		}
	},
	template:`
          <div  class="modal-wrapper" >
            <div ref="modal" class="modal-container" :style="" v-click-outside="closeEvent">

              <div class="modal-header">
                <slot name="header">
                </slot>
              </div>

              <div class="modal-body">
                <slot name="body">
                </slot>
              </div>

              <div class="modal-footer">
                <slot name="footer">
                  <button class="ui-btn ui-btn-xs" @click="$emit('close');el.focus()">
                    OK
                  </button>
                </slot>
              </div>
            </div>
          </div>
	`
});
export{
	modal
}