this.BX = this.BX || {};
(function (exports,ui_vue_router,ui_vue_directives_lazyload,main_polyfill_intersectionobserver,ui_vue) {
	'use strict';

	ui_vue_router = ui_vue_router && ui_vue_router.hasOwnProperty('default') ? ui_vue_router['default'] : ui_vue_router;

	function ready() {
	  document.getElementsByTagName("body")[0].style = 'background-color:#868d95';
	  document.getElementsByClassName('content-wrap')[0].style = 'margin:34px 50px;width:auto';
	  ui_vue.Vue.create({
	    data: {
	      filters: {},
	      table: {
	        head: ['asdasd'],
	        body: {}
	      },
	      urlApi: urlApi
	    },
	    methods: {
	      OpenSideBar: function OpenSideBar(JobId) {
	        // console.log(JobId);
	        BX.SidePanel.Instance.open("Job?id=" + JobId);
	      }
	    },
	    components: {},
	    mounted: function mounted() {
	      var el = document.getElementById('pub-template-error');
	      if (babelHelpers.typeof(el) === 'object') el.style = 'display: none';
	      this.$root.$on('GetJob', this.OpenSideBar);
	    }
	  }).$mount('#app');
	}

	document.addEventListener("DOMContentLoaded", ready);

}((this.BX.Webcodehh = this.BX.Webcodehh || {}),BX,BX,BX,BX));
//# sourceMappingURL=app.bundle.js.map
