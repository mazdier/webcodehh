<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
{
	die();
}

return [
	'css' => 'dist/app.bundle.css',
	'js' => 'dist/app.bundle.js',
	'rel' => [
		'main.polyfill.core',
		'ui.vue.router',
		'ui.vue.directives.lazyload',
		'main.polyfill.intersectionobserver',
		'ui.vue',
		'webcodehh.components.filtershh',
		'webcodehh.components.modal',
		'webcodehh.components.table',
	],
	'skip_core' => true,
];