this.BX = this.BX || {};
(function (exports,main_core) {
	'use strict';

	var WebcodehhApp = /*#__PURE__*/function () {
	  function WebcodehhApp() {
	    var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {
	      name: 'WebcodehhApp'
	    };
	    babelHelpers.classCallCheck(this, WebcodehhApp);
	    this.name = options.name;
	  }

	  babelHelpers.createClass(WebcodehhApp, [{
	    key: "setName",
	    value: function setName(name) {
	      if (main_core.Type.isString(name)) {
	        this.name = name;
	      }
	    }
	  }, {
	    key: "getName",
	    value: function getName() {
	      return this.name;
	    }
	  }]);
	  return WebcodehhApp;
	}();

	exports.WebcodehhApp = WebcodehhApp;

}((this.BX.Webcodehh = this.BX.Webcodehh || {}),BX));
//# sourceMappingURL=webcodehh.app.bundle.js.map
