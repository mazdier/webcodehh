import {Type} from 'main.core';

export class WebcodehhApp
{
	constructor(options = {name: 'WebcodehhApp'})
	{
		this.name = options.name;
	}

	setName(name)
	{
		if (Type.isString(name))
		{
			this.name = name;
		}
	}

	getName()
	{
		return this.name;
	}
}