<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
{
	die();
}

return [
	'css' => 'dist/webcodehh.app.bundle.css',
	'js' => 'dist/webcodehh.app.bundle.js',
	'rel' => [
		'main.core',
	],
	'skip_core' => false,
];