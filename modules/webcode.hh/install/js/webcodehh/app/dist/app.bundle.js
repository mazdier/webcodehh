this.BX = this.BX || {};
(function (exports,ui_vue_router,ui_vue_directives_lazyload,main_polyfill_intersectionobserver,ui_vue) {
       'use strict';

       ui_vue_router = ui_vue_router && ui_vue_router.hasOwnProperty('default') ? ui_vue_router['default'] : ui_vue_router;

       function ready() {
         document.getElementsByTagName("body")[0].style = 'background-color:#868d95';
         document.getElementsByClassName('content-wrap')[0].style = 'margin:34px 50px;width:auto';
         BX.Vue.create({
           data: {
             table: {
               head: ['asdasd'],
               body: {}
             },
             urlApi: urlApi,
             filters: {
               error: false
             }
           },
           beforeMount: function beforeMount() {
             console.log('https://webcode.website' + this.urlApi + 'GetFilters');

             var _self = this;

             BX.ajax({
               url: this.urlApi + 'GetFilters',
               data: {},
               method: 'GET',
               dataType: 'json',
               timeout: 30,
               onsuccess: function onsuccess(filters) {
                 _self.filters = filters;
                 console.log(filters);
               },
               onfailure: function onfailure() {
                 _self.filters = {
                   "error": true
                 };
               }
             });
           }
         }).$mount('#app');
       }

       document.addEventListener("DOMContentLoaded", ready);

}((this.BX.Webcodehh = this.BX.Webcodehh || {}),BX,BX,BX,BX));
//# sourceMappingURL=app.bundle.js.map
