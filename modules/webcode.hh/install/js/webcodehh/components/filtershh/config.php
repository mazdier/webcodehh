<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
{
	die();
}

return [
	'css' => 'dist/filtershh.bundle.css',
	'js' => 'dist/filtershh.bundle.js',
	'rel' => [
		'main.polyfill.core',
		'ui.vue.directives.lazyload',
		'main.polyfill.intersectionobserver',
		'ui.vue',
	],
	'skip_core' => true,
];