this.BX = this.BX || {};
this.BX.Webcodehh = this.BX.Webcodehh || {};
(function (exports,ui_vue_directives_lazyload,main_polyfill_intersectionobserver,ui_vue) {
            'use strict';

            BX.Vue.component('filtershh', {
              template: "\n\t\t <div class=\"ui-ctl ui-ctl-textbox ui-ctl-before-icon ui-ctl-after-icon\">\n            <div class=\"ui-ctl-tag ui-ctl-tag-success\">\u041F\u043E\u0438\u0441\u043A \u0438 \u0444\u0438\u043B\u044C\u0442\u0440\u0430\u0446\u0438\u044F</div>\n            <div class=\"ui-ctl-tag ui-ctl-tag-danger\" >\u041E\u0448\u0438\u0431\u043A\u0430 \u0441\u043E\u0435\u0434\u0438\u043D\u0435\u043D\u0438\u044F</div>\n                <div class=\"ui-ctl-before ui-ctl-icon-search\"></div>\n                <button class=\"ui-ctl-after ui-ctl-icon-clear\"></button>\n                <input type=\"text\" class=\"ui-ctl-element\">\n            </div>\n\t"
            });

}((this.BX.Webcodehh.Components = this.BX.Webcodehh.Components || {}),BX,BX,BX));
//# sourceMappingURL=filtershh.bundle.js.map
