import 'ui.vue.directives.lazyload';
import 'main.polyfill.intersectionobserver';
import {Vue} from 'ui.vue';

BX.Vue.component('filtershh',
{
	template:`
		 <div class="ui-ctl ui-ctl-textbox ui-ctl-before-icon ui-ctl-after-icon">
            <div class="ui-ctl-tag ui-ctl-tag-success">Поиск и фильтрация</div>
            <div class="ui-ctl-tag ui-ctl-tag-danger" >Ошибка соединения</div>
                <div class="ui-ctl-before ui-ctl-icon-search"></div>
                <button class="ui-ctl-after ui-ctl-icon-clear"></button>
                <input type="text" class="ui-ctl-element">
            </div>
	`
});
