<?php
return [
	'userField' => [
        'value' => [
            'access' => '\\Webcode\\HH\\UserField\\UserFieldAccess', 
        ],
    ],
    'controllers' => [
        'value' => [
            'namespaces' => [
                '\\Webcode\\HH\\Controller' => 'api'
            ],
            'defaultNamespace' => '\\Webcode\\HH\\Controller',
        ],
        'readonly' => true
    ]
];
/*class UserField extends \Bitrix\Main\UserField\UserFieldAccess
{
	protected function getAvailableEntitysIds(): array{
		
	}
}
\Bitrix\Intranet\Util::getUserFieldListConfigUrl($moduleId,$entityId);
\Bitrix\Intranet\Util::getUserFieldDetailConfigUrl($moduleId,$entityId,$fieldId);*/