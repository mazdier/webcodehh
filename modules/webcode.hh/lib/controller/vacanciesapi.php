<?php
namespace Webcode\HH\Controller;

use Bitrix\Main\Application;
use Bitrix\Main\Engine\Controller;
use Webcode\HH\Settings;

class VacanciesApi extends Controller
{
    public function __construct(){
        $this->DIC=Settings::getVacanciesSettings();
    }

    public function RunGuestController(){
        $path = self::GetPath();
        file_put_contents(__DIR__."/../../log/Path.txt", $path.PHP_EOL, FILE_APPEND);
        switch ($path){
            case '/GetFilters':
                self::GetApiFilters();
                break;
            case '/GetVacancies':
                self::GetApiVacancies();
                break;
            case '/Job':
                self::GetJob();
                break;
            case '/Vacancy':
                self::GetApiVacancy();
                break;
            case '/IsReview':
                (new \Webcode\HH\Model\Review)->IsReview();
                break;
            case '':
                require_once __DIR__.'/../view/templatevuejs.php';
                break;
            default:
                self::ServiceRequest($path);
                break;
        }
    }
    private function GetPath(){

        preg_match(
            '#(?<=HHApi)\/[a-zA-Z/]+|(?=\?)#is',
            self::GetUrl(),
            $ArPath
        );
        return $ArPath[0];
    }
    private function GetUrl(){
        return Application::getInstance()->getContext()->getServer()->getRequestUri();
    }

    public  function GetApiVacancies(){
        $VacanciesData=[];
        foreach ($this->DIC->get('models') as $model){
            $VacanciesData = array_merge($VacanciesData,$model->GetVacanciesData());
        }
        return $VacanciesData;
    }
    public  function GetJob(){
        $JobData=[];
        foreach ($this->DIC->get('models') as $model){
            $JobData = array_merge($JobData,$model->GetJob());
        }
        return $JobData;
    }

    public  function GetApiVacancy(){
        $VacancyData=[];
        foreach ($this->DIC->get('models') as $model){
            $VacancyData = array_merge($VacancyData,$model->GetVacancy());
        }
        return $VacancyData;
    }

    public  function GetApiFilters(){
        $FiltersData=[];
        foreach ($this->DIC->get('models') as $model){
            $FiltersData = array_merge($FiltersData,$model->GetFilters());
        }
        return $FiltersData;
    }

    public  function ServiceRequest($path){
        $arPath = explode('/',$path);
        $Service = $arPath[1];
        $flag=false;
        foreach ($this->DIC->get('models') as $model){
            if($Service===$model->GetServiceName()){
                $model->ServiceRequest(str_replace('/'.$Service,'',$path));
                $flag = false;
            }
        }
        if ($flag){
            header('Content-Type: application/json');
            return '{"error":true,"error_description":"path not exist"}';
        }
    }
}