<?php
namespace Webcode\HH\Controller;

use Bitrix\Main\Engine\Controller;

class AdminAjax extends Controller
{
    public function configureActions()
    {
        return [
            'save' => [
                'prefilters' => []
            ]
        ];
    }
    public static function saveAction( $data = [])
    {
        return  (new \Webcode\HH\Model\AdminAjax)->SaveOptions($data);
    }
}