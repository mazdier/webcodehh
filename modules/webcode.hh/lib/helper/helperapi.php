<?php
namespace Webcode\HH\Helper;

use Webcode\HH\Settings;

class HelperApi
{
    protected static function GetDI(){
        $DI = Settings::getVacanciesSettings();
       return $DI;
    }
    protected static function CurlRequest($url,$method,$params='',$headers ='',$timeout=10){
        $ch = curl_init();
        if($method==="POST"){
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
        }
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        //curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        $server_output = curl_exec ($ch);
        //print_r(curl_getinfo($ch)['request_header']);
        curl_close ($ch);
        return  $server_output;
    }
    protected static function wrapData($data, $debag = false)
    {
        if(defined('CURRENT_ENCODING')){
            $data = static::changeEncoding($data, true);
        }
        $return = json_encode($data, JSON_HEX_TAG|JSON_HEX_AMP|JSON_HEX_APOS|JSON_HEX_QUOT);
        if($debag){
            $e = json_last_error();
            if ($e != JSON_ERROR_NONE){
                if ($e == JSON_ERROR_UTF8){
                    echo 'Failed encoding! Recommended \'UTF - 8\' or set define CURRENT_ENCODING = current site encoding for function iconv()';
                }
            }
        }
        return $return;
    }
}