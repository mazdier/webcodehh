<?php
namespace Webcode\HH\Model;

interface VacanciesInterface
{
    /**
     * Возвращает имя сервиса
     * @return mixed
     */
    public function GetServiceName();

    /**
     * Возвращает данные вакансий списком
     * @return mixed
     */
    public function GetVacanciesData();

    /**
     * Возвращает фильтры
     * @return mixed
     */
    public function GetFilters();

    /**
     * Запрос на сервер сервиса с заданными параметрами
     * @param string $path
     * @return mixed
     */
    public function ServiceRequest($path);

    /**
     * Возвращает вакансию по идентификатору
     * @return mixed
     */
    public function GetJob();
}