<?php
namespace Webcode\HH\Model;

use Webcode\HH\Helper\HelperApi;
use Webcode\HH\Tools\AuthHH;
use Webcode\HH\Service;

class VacanciesHH extends HelperApi implements VacanciesInterface
{
    protected $token;
    const ServiceName='hh';//headhunter
    public function __construct(AuthHH $AuthObj){
        $this->auth = $AuthObj->GetAuth();
        $this->params = $AuthObj->params;
    }

    public function GetServiceName(){
        return self::ServiceName;
    }

    public function GetVacanciesData(){
        $this->GetRespose()->flush((new Service\VacanciesDataHH(self::GetRequest()))->GetJson());
    }

    public function GetJob(){
        $arRequest = self::GetRequest();
        if(array_key_exists('id',$arRequest)){
            $id = $arRequest['id'];
            require_once __DIR__."/../view/job.php";
        }
        else echo "Такой вакансии не существует";
    }

    public function GetVacancy(){
        $arRequest = self::GetRequest();
        if(array_key_exists('id',$arRequest)){
            $this->GetRespose()->flush( (new Service\VacanciesDataHH())->GetByID($arRequest['id']));
        }
        else echo "Такой вакансии не существует";
    }

    public function GetFilters(){
        $this->GetRespose()->flush((new Service\FilterHH(self::GetRequest()))->GetJson());
    }

    public function ServiceRequest($path){
        $request = self::GetDI()->get('Request');
        $arParams = $request->getQueryList()->toArray();
        $url = $this->params['UrlHHApi'].$path;
        $method = $request->isPost()?'POST':'GET';
        $response = self::CurlRequest($url,$method,$arParams,$this->params['UrlHeader']);
        $this->GetRespose()->flush($response);
    }

    private function GetRespose():\Bitrix\Main\HttpResponse{
        $response = \Bitrix\Main\Application::getInstance()->getContext()->getResponse();
        foreach (self::GetDI()->get('headers') as $header){
            $response->addHeader($header['name'], $header['value']);
        }
        $response->addHeader('Autorization', $header['value']);
        return $response;
    }
    private function GetRequest(){
        return self::GetDI()->get('Request')->getQueryList()->toArray();
    }
}