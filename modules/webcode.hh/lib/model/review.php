<?php
namespace Webcode\HH\Model;

use Webcode\HH\Helper\HelperApi;
use Bitrix\Main\Page\Asset;

class Review extends HelperApi
{
    public function IsReview(){

        $request = $this->GetRequest();
        if(array_key_exists('name',$request)) {
            $response = \Bitrix\Main\Application::getInstance()->getContext()->getResponse();
            $id = $this->FindReview($request['name']);
            if($id){

                $response->flush($this->GetReview($id));
            }
            else $response->flush("Отзывов не найдено");
        }
    }

    private function GetRequest(){
        return self::GetDI()->get('Request')->getQueryList()->toArray();
    }
    private function GetReview($id){
        $Urls = ['https://otrude.net/employers/'.$id,'https://otrude.net/employer-vacancies/'.$id];
        $UrlSite = 'https://otrude.net';
        foreach($Urls as $v){
            $page = file_get_contents($v);
            preg_match('|<div class=\"col-24 col-xl-auto\">(.+)<div class=\"col-24 col-xl-auto ml-xl-auto mb-32 mb-md-48\">|isU',$page , $matches);
            $page = $matches[0];
            //print_r( $matches);
            preg_match_all('|href=\"(.+)\"|isU',$page , $matches);
            foreach($matches[1] as $k => $val){
                static $arrFiltered;
                $pos = strpos($val, $UrlSite);
                if($pos === false){
                    if($val!="/"&&$val!="#"){
                        if(!in_array($val, $arrFiltered)){
                            $arrFiltered[]=$val;
                            $newval = $UrlSite.$val;
                            $page=str_replace($val,$newval,$page);
                        }
                    }
                }
            }
            $page = ' <link rel="stylesheet" href="https://otrude.net/public/css/otrude.min.css?v=1592481508"><div style="margin: 50px">'.$page.'</div>';
            return ' <link rel="stylesheet" href="https://otrude.net/public/css/otrude.min.css?v=1592481508">'.$page;
        }
    }

    private function FindReview($EmploerName){
        $curl = curl_init();
        $data="QWpheE90cnVkZQ%3D%3D%5B%5D=".urlencode($EmploerName) ."&QWpheE90cnVkZQ%3D%3D%5B%5D=otrude.net";
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://otrude.net/ajax/search",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS =>$data ,
            CURLOPT_HTTPHEADER => array(
                "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0",
                "Accept: application/json, text/javascript, */*; q=0.01",
                "Accept-Language: ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3",
                "Accept-Encoding: gzip, deflate, br",
                "Content-Type: application/x-www-form-urlencoded; charset=UTF-8",
                "X-Requested-With: XMLHttpRequest",
                "Content-Length:".strlen($data),
                "Origin: https://otrude.net",
                "Connection: keep-alive",
                "Referer: https://otrude.net/",
                "Cookie: PHPSESSID=kkff92g7pkpjqmkptljb1rsn2e; userInit=0uzr23; location=ru"),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $responce = json_decode($response,true)['response'];
        if(!empty($responce)){
            return $responce[0]['id'];
        }
        else return false;
    }
}
