<?
namespace Webcode\HH\Model;

use Webcode\HH\Settings;
use Bitrix\Main\Config\Option;

class AdminAjax
{
    public function SaveOptions(array $data){
        $MuduleID=Settings::ModuleID;
        foreach ($data as $tabs){
            foreach ($tabs['tab_options'] as $option){
                $option['option_str'];
                Option::set($MuduleID, $option['option_str'], $option['value']);
            }
        }
        $arOptions['state'] = "Save";
        return $arOptions;
    }
}

?>
