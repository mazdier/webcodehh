<?php
namespace Webcode\HH;

use Bitrix\Main\Application;
use Webcode\HH\Tools\DIContainer;

class Settings {
    const settings = ['tabs'=>[
        [
            'tab_name'=> 'Доступ к HeadHunter', 'active'=> true, 'tab_options'=> [
                ['name'=> "Redirect URI", 'option_str'=> "RedirectURI", 'value'=> "",'type'=>'input'],
                ['name'=> "Client ID", 'option_str'=> "ClientID", 'value'=> "",'type'=>'input'],
                ['name'=> "Client Secret", 'option_str'=> "ClientSecret", 'value'=> "",'type'=>'input'],
                ['name'=> "Токен приложения", 'option_str'=> "AppToken", 'value'=> "",'type'=>'input']
            ]
        ],
        /*[
            'tab_name'=> 'Главная2', 'active'=> false, 'tab_options'=> [
                ['name'=>"Логин2", 'option_str'=> "login2", 'value'=> "",'type'=>'input'],
                ['name'=> "Пароль2", 'option_str'=> "password2", 'value'=> "",'type'=>'input'],
                ['name'=> "Чек", 'option_str'=> "check", 'value'=> false,'type'=>'checkbox'],
                ['name'=> "Список", 'option_str'=> "list", 'value'=> "",'list'=> ['1','2'],'type'=>'list']
            ]
        ]*/
    ],
        'buttons'=>[
            [
                'button_name'=>"Сохранить",'button_function'=>'SaveOptions','class'=>'adm-btn-save'
            ],
           /* [
                'button_name'=>"Обновить",'button_function'=>'UpdateOptions','class'=>'adm-btn'
            ]*/
        ]
    ];
    const TableName='b_option';
    const ModuleID='WebcodeHH';
    private static $_instance = null;

    private function __construct() {
    }
    protected function __clone() {
    }
    static public function getInstance() {
        if(is_null(self::$_instance))
        {
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    private static function GetDBOptions(){
        global $DB;
        $Sql='SELECT `name`,`value` FROM '.self::TableName." WHERE `module_id`='".self::ModuleID."'";
        $res = $DB->Query($Sql, false, $err_mess.__LINE__);
        $setting = self::settings;
        while ($row = $res->Fetch())
        {
            foreach ($setting['tabs'] as $k => $tab) {
                foreach ( $tab['tab_options'] as $index => $option) {
                    if($row['name']===$option['option_str']){
                        if($option['type']=='checkbox')
                            $setting['tabs'][$k]['tab_options'][$index]['value']=($row['value']=='false')?false:true;
                        else
                            $setting['tabs'][$k]['tab_options'][$index]['value']=$row['value'];
                    }
                }
            }
        }
        return $setting;
    }
    public function getSettings() {
         return json_encode(self::GetDBOptions());
    }
    public function getVacanciesSettings() {
        static $DIC = null;
        if(!isset($DIC)) {
            $DIC = new DIContainer();
            $DIC->set('models', [
                $DIC->get('Webcode\HH\Model\VacanciesHH', [
                    'params' => [
                        'UrlHHApi' => 'https://api.hh.ru',
                        'UrlAuth' => 'https://hh.ru/oauth/token',
                        'UrlHeader' => [
                            'Content-Type: application/x-www-form-urlencoded',
                            'User-Agent: WebcodeHH/0.1 (mazdiers@gmail.com)',
                            'Host: api.hh.ru',
                            'Accept: */*'
                        ],
                    ],
                ]),
            ]);
            $DIC->set('headers', [
                ['name' => 'Content-Type', 'value' => 'application/json'],
                ['name' => 'Access-Control-Allow-Credentials', 'value' => 'true'],
                ['name' => 'Access-Control-Allow-Origin', 'value' => 'https://app.swaggerhub.com'],
            ]);
            $DIC->set("Request",Application::getInstance()->getContext()->getRequest());
        }
        return $DIC;
    }
}