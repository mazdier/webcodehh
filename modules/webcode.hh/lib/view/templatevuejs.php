<?php
use Bitrix\Main\Page\Asset;

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

\Bitrix\Main\UI\Extension::load('webcodehh.app');
\CJSCore::init("sidepanel");
Asset::getInstance()->addCss("/bitrix/css/main/bootstrap.css");

?>

<div id="app">
    <filtershh :urlApi="urlApi"></filtershh>
    <tablehh :urlApi="urlApi"></tablehh>
</div>
<script>
    let urlApi="<?=self::GetUrl()?>";

</script>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
