
<link rel="stylesheet" href="https://i.hh.ru/css/globals/pages/__bloko-imports_3c2e99df8215c068830e0c8cbccc805b.css">

<?php
header("Content-Type: text/html");
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');
$APPLICATION->SetTitle('Bitrix Vue Component');

//переменная $id из Webcode\HH\Service\Model::GetJob

Webcode\HH\Vue::includeComponent([
    'jobhh',
]);
?>

<main id="app" style="margin:50px">
    <jobhh :id="<?=$id?>">
    </jobhh>
</main>

<script>
    new Vue({
        el: '#app',
        name: 'root',
        data:{
        },
        mounted: function () {
        }
    })
</script>



<? require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php'); ?>
