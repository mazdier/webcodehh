<?php
namespace Webcode\HH\Service;

use Webcode\HH\Helper\HelperApi;
use Webcode\HH\Tools\UserOptions;

class FilterHH extends HelperApi implements FilterInterface{
    private $data;
    const ServiceName='hh';
    public function __construct($arFilter)
    {
        $this->settings=self::GetDI()->get('Webcode\HH\Tools\AuthHH')->params;
        if(!$arFilter)
            $this->arFilters = UserOptions::getFilterOptions();
        else $this->arFilters = $arFilter;
    }

    public function GetJson(){
        $ar = self::GetArray();
        $ar['error']=$ar?false:true;
        return json_encode($ar);
    }

    public function GetArray(){

        return $this->GetRemakeData();
    }

    public function GetByName($name){

    }
    public function GetRemakeData(){
        $data = json_decode($this->GetData(),true);
        foreach ($this->arFilters[self::ServiceName] as $k => $v){
            if(!array_key_exists('data',$v))
                if(array_key_exists($k,$data)){
                    switch ($this->arFilters[self::ServiceName][$k]['type']){
                        case 'currency':
                            foreach ($data[$k] as $datum) {
                                static $d=[];
                                $d[]=['id'=>$datum['code'],'name'=>$datum['name'],'rate'=>$datum['rate']];
                            }
                            $this->arFilters[self::ServiceName][$k]['data'] = $d;
                            break;
                        default:
                            $this->arFilters[self::ServiceName][$k]['data'] = $data[$k];
                    }
                }
        }
        return $this->arFilters[self::ServiceName];
    }

    public function GetData(){
        $url = $this->settings['UrlHHApi'].'/dictionaries';
        $response = self::CurlRequest($url,'GET',[],$this->settings['UrlHeader']);
        return $response;
    }
}
