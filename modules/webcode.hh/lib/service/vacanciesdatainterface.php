<?php
namespace Webcode\HH\Service;

interface VacanciesDataInterface
{
    /**
     * Возвращает данные в json формате
     * @return mixed
     */
    public function GetJson();

    /**
     * Возвращает данные в формате массива
     * @return mixed
     */
    public function GetArray();

    /**
     * Возвращает вакансию по идентификатору
     * @param string $id
     * @return mixed
     */
    public function GetByID($id);
}