<?php
namespace Webcode\HH\Service;

interface FilterInterface
{
    /**
     * Возвращает данные в json формате
     * @return mixed
     */
    public function GetJson();

    /**
     * Возвращает данные в формате массива
     * @return mixed
     */
    public function GetArray();

    /**
     * Возвращает фильтр по идентификатору
     * @param string $id
     * @return mixed
     */
    public function GetByName($id);
}