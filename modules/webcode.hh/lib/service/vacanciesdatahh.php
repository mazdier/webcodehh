<?php
namespace Webcode\HH\Service;

use Webcode\HH\Helper\HelperApi;
use Webcode\HH\Tools\UserOptions;

class VacanciesDataHH extends HelperApi implements VacanciesDataInterface{
    private $data;
    const ServiceName='hh';
    public function __construct()
    {
        $this->settings=self::GetDI()->get('Webcode\HH\Tools\AuthHH')->params;
        $this->arFilters = UserOptions::getVacanciesOptions();
    }

    public function GetJson(){
        return json_encode($this->RemakeForApi());
    }

    public function GetArray(){

        return $this->GetRemakeData();
    }

    public function GetByID($id){
       return self::GetDataJob($id);
    }

    public function GetRemakeData(){
        $this->newData['items']=[[]];
        $data = json_decode($this->GetDataVacansies(),true);
        $filter = $this->arFilters[self::ServiceName];
		$headers = $filter['TableHeaders'];
		unset($filter['TableHeaders']);
        foreach ($data['items'] as $key => $job) {
            self::PrepareArray($job, $filter, $key);
        }
		$this->newData['headers'] = $headers;
        $this->newData['found'] = $data['found'];
        $this->newData['pages'] = $data['pages'];
        $this->newData['per_page'] = $data['per_page'];
        $this->newData['page'] = $data['page'];
        $this->newData['clusters'] = $data['clusters'];

        return $this->newData;
    }
    public function RemakeForApi(){
        $data = $this->GetRemakeData();
        foreach ($data['items'] as $key => $job) {
            $this->newData['items'][$key]['contenthh']='';
            foreach ($job as $k => $v){
                if($v['type']==='contenthh'){
                    unset($this->newData['items'][$key][$k]);
					if(!$this->newData['items'][$key]['contenthh'])
						$this->newData['items'][$key]['contenthh'] = ["type"=>"contenthh","data"=>[]];
                    $this->newData['items'][$key]['contenthh']['data'] [] = $v['data'].'</br>';
                }
            }
        }
        return $this->newData;
    }

    public function GetDataVacansies(){
        $url = $this->settings['UrlHHApi'].'/vacancies?'.http_build_query(
                self::GetDI()->get('Request')->getQueryList()->toArray()
            );
        $response = self::CurlRequest($url,'GET',[],$this->settings['UrlHeader']);
        return $response;
    }
    public function GetDataJob($id){
        $url = $this->settings['UrlHHApi'].'/vacancies/'.$id.'?host=hh.ru';
        $response = self::CurlRequest($url,'GET',[],$this->settings['UrlHeader']);
        return $response;
    }

    private function PrepareArray($data,$filter,$index){
        if(array_key_exists($index,$this->newData['items'])) {
            $this->newData['items'][$index] = array_merge($this->newData['items'][$index],$filter);
        }
        else
            $this->newData['items'][$index] = $filter;
        foreach ($filter as $k => $v) {
            switch ($filter[$k]['type']){
                case 'array':
                    unset($this->newData['items'][$index][$k]);
                    self::PrepareArray($data[$k],$filter[$k]['array'],$index);
                    break;
                case 'currency':
                    if($data[$k]['currency'])
                        $this->newData['items'][$index][$k] = array_merge($this->newData['items'][$index][$k],['data'=>
                            [
                                'id'=>$data[$k]['currency'],
                                'amount'=>$data[$k]['from'],
                            ]
                        ]);
                    break;
				case 'urlvac':
                    if($data[$k]['urlvac'])
                        $this->newData['items'][$index][$k] = array_merge($this->newData['items'][$index][$k],['data'=>
                            [
                                'url'=>$data[$k],
                                'name'=>$data['name'],
                                'id'=>$data['id'],
                            ]
                        ]);
                    break;
                case 'employer':
                    $this->newData['items'][$index][$k] = array_merge($this->newData['items'][$index][$k],['data'=>
                        [
                            'id'=>$data[$k]['id'],
                            'name'=>$data[$k]['name'],
                            'logo'=>$data[$k]['logo_urls']['90'],
                        ]
                    ]);
                    break;
                default:
                    $this->newData['items'][$index][$k] = array_merge($this->newData['items'][$index][$k],['data'=>$data[$k]]);
            }
        }
    }
}
