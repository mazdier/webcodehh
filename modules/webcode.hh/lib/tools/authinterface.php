<?php
namespace Webcode\HH\Tools;

interface AuthInterface
{
    /**
     *  метод для получения авторизации сервиса
     */
    public function GetAuth();
}