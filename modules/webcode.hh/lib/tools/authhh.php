<?php
namespace Webcode\HH\Tools;

use Webcode\HH\Helper\HelperApi;
use Bitrix\Main\Config\Option;


class AuthHH extends HelperApi implements AuthInterface
{
    static $UrlHeader=[],
        $UrlHHApi='',
        $UrlAuth='';
    public function __construct($params){
        $this->params=$params;
        self::$UrlHeader=$params['UrlHeader'];
        self::$UrlHHApi=$params['UrlHHApi'];
        self::$UrlAuth=$params['UrlAuth'];
    }

    public function GetAuth()
    {
        if(!empty($auth=self::GetAuthData())){
            $auth=json_decode($auth,true);
            if(array_key_exists('token_type',$auth))
                switch($auth['token_type']){
                    case 'bearer':
                        $this->params['UrlHeader'] = array_merge(self::$UrlHeader, ['Authorization: Bearer ' . $auth['access_token']]);
                        break;
                    default:
                        $this->params['UrlHeader'] = array_merge(self::$UrlHeader, ['Authorization: Bearer ' . $auth['access_token']]);
                }
            /*$responce = self::CurlRequest(self::$UrlHHApi . '/me',
                "GET",
                self::GetOptionsAuth(),
                $header
            );
            if(array_key_exists('is_application',$responce=json_decode($responce,true)))
                $auth=($responce['is_application']===true)?$auth:self::GetToken();
            else $auth=false;*/
        }
        else{
            $auth = self::GetToken();
        }
        return $auth;
    }

    protected static function GetToken()
    {
        $responce = self::CurlRequest(self::$UrlAuth,
            "POST",
            self::GetOptionsAuth(),
            self::$UrlHeader
        );
        if(is_array($responce))
            if(array_key_exists('access_token')){
                self::SetAuthData($responce);
                return $responce;
            }
            else return false;
        else return false;
    }

    protected static function SetAuthData($arSettings)
    {
        return  (boolean)file_put_contents(__DIR__ . '/../auth.json', static::wrapData($arSettings));
    }

    protected static function GetAuthData()
    {
        return  file_get_contents(__DIR__ . '/../auth.json');
    }

    protected static function GetOptionsAuth(){
        $params=[
            'grant_type'=>'client_credentials',
            'client_id'=>Option::get(\Webcode\HH\Settings::ModuleID, "ClientID"),
            'client_secret'=>Option::get(\Webcode\HH\Settings::ModuleID, "ClientSecret"),
        ];
        return $params;
    }
}