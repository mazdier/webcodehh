<?php
namespace Webcode\HH\Tools;
use Webcode\HH\Model;

class DIContainer
{
    public $instances = [];
    public function set($abstract, $variable = NULL){
        if ($variable === NULL) {
            $variable = $abstract;
        }
        $this->instances[$abstract] = $variable;
    }
    public function &get($abstract, $parameters = [],$flagNew=false){
        //если нету - создать
        $this->flagNew=$flagNew;
        if (!isset($this->instances[$abstract])) {
            return 	$this->resolve($abstract, $parameters);
        }
        else {
            return $this->flagNew?$this->resolve($this->instances[$abstract], $parameters):$this->instances[$abstract];
        }
    }
    private function resolve($concrete, $parameters)
    {

        $reflector = new \ReflectionClass($concrete);

        // создан ли экземпляр класса
        if (!$reflector->isInstantiable()) {
            throw new Exception("Class {$concrete} is not instantiable");
        }
        $constructor = $reflector->getConstructor();
        if (is_null($constructor)) {
            $this->set($concrete,$obj=$reflector->newInstance());
            // Новый экзепляр  класса
            return $obj;
        }
        $objParameters   = $constructor->getParameters();
        $dependencies = $this->getDependencies($objParameters,$parameters);
        $this->set($concrete,$obj=$reflector->newInstanceArgs($dependencies));
        // Новый экзепляр класса c параметрами
        return $obj;
    }
    private function getDependencies($objParameters,$parameters)
    {
        $dependencies = [];
        foreach ($objParameters as $parameter) {
            $dependency = $parameter->getClass();
            if ($dependency === NULL) {
                if (count($parameters)===0){
                    // доступно ли значение параметра по умолчанию
                    if ($parameter->isDefaultValueAvailable())
                        $dependencies[] = $parameter->getDefaultValue();
                    else throw new Exception("Undefined class dependency {$parameter->name}");
                }
                else {
                    $flag=true;
                    foreach($parameters as $k => $v){
                        if ($parameter->name === $k){
                            $dependencies[$k]=$v;
                            $flag=false;
                        }
                    }
                    if ($flag) {
                        // доступно ли значение параметра по умолчанию
                        if ($parameter->isDefaultValueAvailable())
                            $dependencies[] = $parameter->getDefaultValue();
                        else throw new Exception("Undefined class dependency {$parameter->name}");
                    }
                }
            } else {
                $dependencies[] = $this->get($dependency->name,$parameters,$this->flagNew);
            }
        }
        return $dependencies;
    }
}
