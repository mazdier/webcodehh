<?php
namespace Webcode\HH\Tools;

class UserOptions {
    protected static $_instance;

    private function __construct() {
    }

    public static function getInstance() {
        if (self::$_instance === null) {
            self::$_instance = new self;
        }

        return self::$_instance;
    }

    private function __clone() {
    }

    private function __wakeup() {
    }
    private function getDefaultFilterOptions(){
        $arOptions=[
            'hh'=>[
                'experience'=>['type'=>'dictionaries','name'=>'опыт работы'],
                'employment'=>['type'=>'dictionaries','name'=>'тип занятости'],
                'schedule'=>['type'=>'dictionaries','name'=>'график работы'],
                'currency'=>['type'=>'currency','name'=>'валюта'],
                'salary'=>['type'=>'int','name'=>'размер заработной платы','data'=>[]],
                'period'=>['type'=>'daypicker','name'=>'период в днях','data'=>''],
                'vacancy_search_order'=>['type'=>'dictionaries','name'=>'сортировка списка вакансий'],
                'premium'=>['type'=>'boolean','name'=>'премиум вакансии'],
            ]
        ];
        return $arOptions;
    }
    private function getUserFilterOptions(){
        $arOptions=[
            'hh'=>[
                'experience',
                'employment',
                'schedule',
                'currency',
                'salary',
                'period',
                'vacancy_search_order',
                'premium',
            ]
        ];
        return $arOptions;
    }
    public function getFilterOptions() {
        $DArOptions = self::getDefaultFilterOptions();
        $UArOptions = self::getUserFilterOptions();
        $arRezult=[];
        foreach ($DArOptions as $k => $v){
            $arRezult[$k] = array_filter(
                $v,
                function ($key) use ($UArOptions,$k) {
                    return in_array($key, $UArOptions[$k]);
                },
                ARRAY_FILTER_USE_KEY
            );
        }
        return $arRezult;
    }
    private function getDefaultJobOptions(){
        $arOptions=[
            'hh'=>[
                'experience'=>['type'=>'dictionaries','name'=>'опыт работы'],
                'employment'=>['type'=>'dictionaries','name'=>'тип занятости'],
                'schedule'=>['type'=>'dictionaries','name'=>'график работы'],
                'salary'=>['type'=>'int','name'=>'размер заработной платы','data'=>[]],
                'order_by'=>['type'=>'dictionaries','name'=>'сортировка списка вакансий'],
                'premium'=>['type'=>'boolean','name'=>'премиум вакансии'],
            ]
        ];
        return $arOptions;
    }
    private function getUserJobOptions(){
        $arOptions=[
            'hh'=>[
                'experience',
                'employment',
                'schedule',
                'salary',
                'order_by',
                'premium',
            ]
        ];
        return $arOptions;
    }
    public function getJobOptions() {
        $DArOptions = self::getDefaultJobOptions();
        $UArOptions = self::getUserJobOptions();
        $arRezult=[];
        foreach ($DArOptions as $k => $v){
            $arRezult[$k] = array_filter(
                $v,
                function ($key) use ($UArOptions,$k) {
                    return in_array($key, $UArOptions[$k]);
                },
                ARRAY_FILTER_USE_KEY
            );
        }
        return $arRezult;
    }
    public function getVacanciesOptions() {
        $arRezult=[
            'hh'=>[
				'TableHeaders'=>[
					'id'=>'Идентификатор',
					 'name'=>'Название',
					 'salary'=>'Валюта',
					  'employer'=>'Работодатель',
					   'url'=>'Ссылка',
					  'contenthh'=>'Требования,Обязанности',
					 
				],
                'id'=>['type'=>'id'],
                'name'=>['type'=>'string'],
                'salary'=>['type'=>'currency'],
                'employer'=>['type'=>'employer'],
                'snippet'=>['type' => 'array', 'array'=>[
                        'requirement'=>['type'=>'contenthh'],
                        'responsibility'=>['type'=>'contenthh'],
                    ],
                ],
                'url'=>['type'=>'urlvac']
            ]
        ];
        return $arRezult;
    }
}