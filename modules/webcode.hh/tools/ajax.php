<?php
use \Bitrix\Main\HttpRequest;
define('NO_KEEP_STATISTIC', 'Y');
define('NO_AGENT_STATISTIC','Y');
define('NO_AGENT_CHECK', true);
define('DisableEventsCheck', true);

require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
if(!defined('PUBLIC_AJAX_MODE'))
{
    define('PUBLIC_AJAX_MODE', true);
}
print_r($_REQUEST);