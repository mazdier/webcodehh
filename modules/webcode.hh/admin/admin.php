<?
use WebcodeHHAdmin\Settings;
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/prolog.php");
global $APPLICATION;
$APPLICATION->SetTitle(GetMessage("OPTIONS_TITLE"));
require_once($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/include/prolog_admin_after.php");
\Bitrix\Main\UI\Extension::load("ui.vue");

?>
    <script type="text/x-template" id="tab-content">
        <div>
                <table>
                    <thead>
                    </thead>
                    <tbody>
                    <tr v-for="(option,index) in tab.tab_options">
                        <td>
                            {{option.name}}:
                        </td>
                        <td>
                            <input v-if="option.type==='input'" v-model="option.value" class="adm-input" size="86">
                            <input v-if="option.type==='checkbox'" type="checkbox" :name="index" v-model="option.value">
                            <select v-if="option.type==='list'" v-model="option.value">
                                <option v-for="val in option.list">{{val}}</option>
                            </select>
                        </td>
                    </tr>
                    </tbody>
                </table>
        </div>
    </script>
    <div id="app">
        <div id="tabControl_tabs" style="left: 0px;" class="adm-detail-tabs-block">
            <span  v-for="tab in tabs"
                   :class="{'is-active': tab.active,'adm-detail-tab-active':tab.active}"
                   class="adm-detail-tab  adm-detail-tab-last"
                   @click="setActive(tab)"
            >
                {{tab.tab_name}}
            </span>
        </div>
        <div class="adm-detail-content-wrap">
            <div class="adm-detail-content">
                <div class="adm-detail-title">{{StateOption}}</div>
                <div class="adm-detail-content-item-block">
                    <tab-content class="content" :tab="currentTab"/>
                </div>
                <div class="adm-detail-content-btns-wrap" id="tabControl_buttons_div" style="left: 0px;">
                    <div class="adm-detail-content-btns">
                            <input
                                    v-for="(button,index) in buttons"
                                    type="button"
                                    :value="button.button_name"
                                    @click="LoadFunction(index)"
                                    :class="button.class"
                            >
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
       let data=JSON.parse('<?=\Webcode\HH\Settings::getSettings()?>');
       console.log(data);
       BX.Vue.component('tab-content', {
           template: '#tab-content',
           props: ['tab']
       });
       BX.Vue.create({
           el: '#app',
           data: {
               tabs:data.tabs,
               buttons:data.buttons,
               StateOption:''
           },
           methods: {
               setActive(tab) {
                   this.tabs.forEach(el => {
                       el.active = el === tab;
                   })
               },
               LoadFunction(index){
                   eval('this.'+this.buttons[index].button_function+'(this.tabs)');
               },
               SaveOptions(data){
                   var request = BX.ajax.runAction('webcode:hh.api.adminajax.save', {
                       data: {
                           data: data
                       }
                   });
                    _self=this;
                   request.then(function(response){
                       _self.StateOption = response.data.state;
                       console.log(response);
                   });
               },
               UpdateOptions(data){
                   console.log('UpdateOptions')
               }
           },
           computed: {
               currentTab: function () {
                   return this.tabs.reduce((accum, curr) => {return curr.active ? curr : accum}, {});
               }
           }
       });
    </script>
<?require_once ($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/include/epilog_admin.php");?>