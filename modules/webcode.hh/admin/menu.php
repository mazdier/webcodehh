<?
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

if ($APPLICATION->GetGroupRight('conversion') == 'D')
{
	return false;
}
else
{
	$menu = array(
		array(
			'parent_menu' => 'global_menu_settings',
			'sort' => 100,
			'text' => 'WebcodeHH',
			'title' => 'Интеграция c HH.ru',
			'icon' => 'excel_menu_icon',
			'page_icon' => 'excel_menu_icon',
			'items_id' => 'menu_excel',
			'url' => 'webcodeHH.php',
			'module_id' => 'webcode_hh'
//			'items' => array(
//				array(
//					'text' => Loc::getMessage('CONVERSION_MENU_SUMMARY_TEXT'),
//					'title' => Loc::getMessage('CONVERSION_MENU_SUMMARY_TEXT'),
//					'url' => 'conversion_summary.php?lang='.LANGUAGE_ID,
//				),
//			),
		),
	);

	return $menu;
}
?>